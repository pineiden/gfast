# standar library
import asyncio
import concurrent.futures
import functools
import math
import sys
import time

# multiprocessing module
from multiprocessing import Manager, Queue, Lock
from multiprocessing.managers import BaseManager, SyncManager

# args parse
import argparse

# module tasktools
from tasktools.taskloop import coromask, renew, simple_fargs
from tasktools.assignator import TaskAssignator


# module networktools 
from networktools.colorprint import gprint, bprint, rprint
from networktools.ports import used_ports, get_port
from networktools.ssh import bridge, kill


# module GNCSocket Clientt
from socket_client import SocketClient

# GFAST FUNCTIONS
from GFAST_paraminit import Properties
from GFAST2 import gfast_run_task

if __name__ == "__main__":
    # collector address, port
    workers = 3
    loop = asyncio.get_event_loop()
    executor = concurrent.futures.ProcessPoolExecutor(workers)
    manager = Manager()
    user='geodesia'
    collector_host='10.54.217.15'
    gnsocket_port=6677

    # GNCSocket port
    port = gnsocket_port
    up = used_ports()
    [local_port, up] = get_port(up)
    address = ('localhost', local_port)

    # Bridge to datawork redis
    ssh_bridge = bridge(local_port, port, collector_host, user)

    kwargs = {
        'address': address,
        'stations': manager.dict(),
        'position': manager.dict(),
        'db_data': manager.dict(),
        'read_queue': manager.Queue(),
        'write_queue': manager.Queue()
    }
    #bprint("Socket client input %s" %kwargs )
    socket_client = SocketClient(**kwargs)

    # GFAST INITIALIZE

    props = Properties('./gfast.props')
    #print("Properties %s" % props.dict_prop)


    # create tasks
    tasks_list = []
    networktask = loop.run_in_executor(
        executor,
        socket_client.msg_network_task
    )
    tasks_list.append(networktask)

    gfast_task = loop.run_in_executor(
        executor,
        functools.partial(gfast_run_task, props, kwargs)
    )
    tasks_list.append(gfast_task)

    loop.run_until_complete(asyncio.gather(*tasks_list))
