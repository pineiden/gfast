#!/usr/bin/python
import pika
from GFAST_paraminit import Properties
from GFAST_buffer import bufferinitnew, bufferupdatenew
import time
import json

def callbackeqmessage(ch, method, properties, body):
	hypomessage = json.loads(body)
	print (body)


RMQequser = 'gfast'
RMQeqpassword = 'foobarbaz'
RMQeqexchange = 'amq.fanout'
RMQeqhost = 'hypo'
RMQeqtopic = 'shogouki.csn.uchile.cl'
RMQeqport = '5672'
credentialseq = pika.PlainCredentials(RMQequser,RMQeqpassword)
parameterseq = pika.ConnectionParameters(RMQeqtopic,RMQeqport,RMQeqhost,credentials=credentialseq)
connectioneq = pika.BlockingConnection(parameterseq)

channeleq = connectioneq.channel()
channeleq.exchange_declare(exchangeeq=RMQeqexchange,passive=True)
queue_nameeq = 'gfast'
channeleq.queue_bind(exchangeeq=RMQeqexchange,queue=queue_nameeq,routing_key='gfast')

channeleq.basic_consume(callbackeqmessage,queue=queue_nameeq,no_ack=True)
channeleq.start_consuming()



