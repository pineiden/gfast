
#Overall G-FAST properties
properties = dict(
    streamfile="GFAST_streams.txt",
    eewsfile="ElarmS_messages.txt",
    eewgfile="GFAST_output.txt",
    siteposfile="site_pos.csv", 
    bufflen=300, 
    #PGD Properties
    #CMT/FF Properties
    #ActiveMQ Properties
    AMQhost="siren",
    AMQport=61613,
    AMQtopic="eew.alg.elarms.data",
    AMQuser="elarms",
    AMQpassword="pnsnIsOK",
    #RabbitMQ Properties
    #RMQhost=/CWU-ppp
    #RMQport=5672
    #RMQtopic=www.panga.org
    #RMQuser=uw
    #RMQpassword=MynManAp
    #RMQexchange=nev-cov
    RMQhost="gfast",
    RMQuser="gfast",
    RMQport=5672,
    RMQpassword="xquake18",
    RMQexchange="amq.topic",
    RMQtopic="10.54.218.66",
    RMQqueue="datawork2gfast",
    #Synthetic mode (0 for no, 1 for yes)
    synmode=0,
    syndriver="nisqually_driver.txt",
    synoutput="nisqually_output.txt"
)
