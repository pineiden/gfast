import asyncio
import functools


# TaskTools for concurrency loop
from tasktools.taskloop import coromask, renew, simple_fargs, simple_fargs_out
from tasktools.scheduler import TaskScheduler

# Async Socket
from gnsocket.gn_socket import GNCSocket

# NetworkTools
from networktools.colorprint import gprint, bprint, rprint
from networktools.library import (pattern_value, fill_pattern, context_split,
                                  gns_loads, gns_dumps)
from networktools.geo import (radius, rad2deg, deg2rad,
                              ecef2llh, llh2ecef, get_from_ecef, ecef2neu)

# multiprocessing part
from multiprocessing import Lock
from multiprocessing import Manager, Queue, Lock

import simplejson as json

class SocketClient:
    def __init__(self, *args, **kwargs):
        self.collector_address = kwargs.get('address',('localhost',6666))
        self.rq = kwargs.get('read_queue',Queue())
        self.wq = kwargs.get('write_queue',Queue())
        self.stations = kwargs.get('stations',{})
        self.position = kwargs.get('position',{})
        self.db_data = kwargs.get('db_data',{})
        self.queue_list = (self.rq, self.wq)
        self.start = 0
        #bprint("Position in socket %s" %self.position)

    async def recv_msg(self, loop, idc):
        ans = []
        gs = self.gs
        #print(gs)
        msg = ''
        while not msg == '<END>':
            msg = await gs.recv_msg(idc)
            if msg != '<END>' and msg is not None:
                ans.append(msg)
            if msg == '<END>':
                break
        return ans

    async def load_stations(self, idc):
        get_lst = "ADMIN|ADMIN|GET|LST|STA"
        gs = self.gs
        await gs.send_msg(get_lst, idc)
        loop = asyncio.get_event_loop()
        #print("Station loop")
        try:
            msg = await self.recv_msg(loop, idc)
        except Exception as exc:
            print(exc)
            raise exc
        #bprint("MSG recibido")
        qs = dict()
        POSITION = dict()
        for m in msg:
            #print(m)
            msg_list = m.split('|')
            qs = gns_loads(msg_list[4])
            #print(qs)
        for ids in qs:
            qs[ids]['STATUS'] = 'OFF'
            POSITION[ids] = dict()
            POSITION[ids]['ECEF'] = dict()
            #print("KEYS %s" % POSITION.keys())
        for ids in qs:
            self.stations[ids] = qs[ids]
            if 'ECEF_Z' in qs[ids]:
                Z = qs[ids]['ECEF_Z']
                #print(type(POSITION[ids]))
                POSITION[ids]['ECEF'].update({'Z': Z})
                #gprint("Z: %s" % Z)
                #gprint("Type %s" % type(POSITION[ids]['ECEF']))
                #gprint("Z en position %s" % POSITION[ids]['ECEF'].keys())
            if 'ECEF_X' in qs[ids]:
                X = qs[ids]['ECEF_X']
                #print(type(POSITION[ids]))
                POSITION[ids]['ECEF'].update({'X': X})
                #gprint("X: %s" % X)
                #gprint("Type %s" % type(POSITION[ids]['ECEF']))
                #gprint("X en position %s" % POSITION[ids]['ECEF'].keys())
            if 'ECEF_Y' in qs[ids]:
                Y = qs[ids]['ECEF_Y']
                #print(type(POSITION[ids]))
                POSITION[ids]['ECEF'].update({'Y': Y})
                #gprint("Y: %s" % Y)
                #gprint("Type %s" % type(POSITION[ids]['ECEF']))
                #gprint("Y en position %s" % POSITION[ids]['ECEF'].keys())
            if 'position' in qs[ids]:
                pst = json.loads(qs[ids]['position'])
                coords = pst['coordinates']
                [lat, lon] = deg2rad(*coords)
                #gprint("Station data:")
                #bprint("Data from station %s :" % self.stations[ids]['code'])
                POSITION[ids].update({'lat': lat})
                POSITION[ids].update({'lon': lon})
                POSITION[ids].update({'radius': radius(lat)[0]})
                XYZ = llh2ecef(lat, lon, Z)
                # bprint(XYZ) ok, correct
                #bprint(POSITION[ids].keys())
                POSITION[ids].update({'ECEF': dict(zip(['X', 'Y', 'Z'], XYZ))})
            x = POSITION[ids]['ECEF']['X']
            y = POSITION[ids]['ECEF']['Y']
            z = POSITION[ids]['ECEF']['Z']
            (lat, lon, h) = ecef2llh(x, y, z)
            POSITION[ids].update({'llh':{'lat': lat,'lon':lon,'z': h}})
            self.position[ids] = POSITION[ids]
            #rprint("Position absoluta")
            #rprint(self.position[ids].keys())
            if 'code' in qs:
                self.common[qs][ids] = dict()

    async def load_dbdata(self, idc):
        get_lst = "ADMIN|ADMIN|GET|LST|DB"
        gs = self.gs
        await gs.send_msg(get_lst, idc)
        loop = asyncio.get_event_loop()
        msg = await self.recv_msg(loop, idc)
        qs = dict()
        for m in msg:
            #print(m)
            msg_list = m.split('|')
            qs = gns_loads(msg_list[4])
            #print(qs)
        for k in qs:
            self.db_data[k] = qs[k]


    async def load_data(self, *args):
        """
        Load main data at the beggining
        In the future, must handle messages betwen
        DragonDataWork and Collector

        """
        loop = asyncio.get_event_loop()
        gs = self.gs
        idc = args[0]
        #rprint("Self start %s" %self.start)
        if self.start == 0:
            bprint("Loading data from collector")
            idc = await gs.create_client()
            await self.receive_wc(idc)
            await self.load_stations(idc)
            await self.load_dbdata(idc)
            stations = self.stations
            #bprint(stations)
            #self.queue_list.put(stations)
            # send ids to queue
            await asyncio.sleep(3)
            #print(self.stations)
            self.start = 1
            stations=self.stations
            self.wq.put("TRANSFORM")
        else:
            #bprint("Wait sleep on fn load_data")
            #rprint("Stations")
            #gprint(self.stations)
            #rprint("Positions")
            #bprint(self.position)
            await asyncio.sleep(25)


    def load_data_task(self, loop, idc):
        args = [idc]
        task = loop.create_task(coromask(self.load_data,
                                         args,
                                         simple_fargs))
        task.add_done_callback(functools.partial(renew,
                                                 task,
                                                 self.load_data,
                                                 simple_fargs))
        if not loop.is_running():
            loop.run_forever()


    def msg_network_task(self):
        #gprint("Rescatando queues rq wq")
        # queue_list = [self.rq, self.wq]
        self.gs = GNCSocket(mode='client')
        self.gs.set_address(self.collector_address)
        loop = asyncio.new_event_loop()
        # client mode to obtain idc
        gs = self.gs
        gs.set_loop(loop)
        idc = loop.run_until_complete(gs.create_client())
        self.load_data_task(loop, idc)

    async def receive_wc(self, idc):
        loop = asyncio.get_event_loop()
        msg = await self.recv_msg(loop, idc)
        for m in msg:
            print(m)
        return msg
