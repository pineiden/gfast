#!/usr/bin/python
from gfast_prop import properties

class Properties:
    def __init__(self, propfilename):
        #print("Init properties")    
        self.dict_prop = {}
        #print("Name of the file : %s" %propfilename)
        """
        infile = open(propfilename, 'r')
        for line in infile:
            print(line)
            if (line) and (line[0] != '#') and ('=' in line):
                (key, val) = line.split('=')
                self.dict_prop[key] = val.strip()
            return
        """
        self.dict_prop=properties
        self.dict_prop['streamfile'] = propfilename

    def getAMQhost(self):
        return self.dict_prop.get('AMQhost','')

    def getAMQport(self):
        return self.dict_prop.get('AMQport',0)
        
    def getAMQtopic(self):
        return self.dict_prop.get('AMQtopic','')

    def getAMQuser(self):
        return self.dict_prop.get('AMQuser','')

    def getAMQpassword(self):
        return self.dict_prop.get('AMQpassword','')

    def getstreamfile(self):
        return self.dict_prop.get('streamfile','')

    def geteewsfile(self):
        return self.dict_prop.get('eewsfile','')

    def geteewgfile(self):
        return self.dict_prop.get('eewgfile','')

    def getsiteposfile(self):
        return self.dict_prop.get('siteposfile','')
        
    def getbufflen(self):
        #print("Bufflend")
        #print(self.dict_prop['bufflen'])
        return self.dict_prop.get('bufflen',0)

    def getstreamlength(self):
        return 100
        """
        if self.dict_prop.get('streamfile',''):
            sf = self.dict_prop['streamfile']
            with open(sf) as f:
                for i, l in enumerate(f):
                    pass
                streamlength = i+1
            return streamlength
        """

    def getmode(self):
        return self.dict_prop.get('synmode','')

    def getsyndriver(self):
        return self.dict_prop.get('syndriver','')

    def getsynoutput(self):
        return self.dict_prop.get('synoutput','')

    def getRMQhost(self):
        print("Getting rmq HOST %s" %self.dict_prop.get('RMQhost',''))
        return self.dict_prop.get('RMQhost','')

    def getRMQport(self):
        return self.dict_prop.get('RMQport',0)

    def getRMQtopic(self):
        return self.dict_prop.get('RMQtopic','')

    def getRMQuser(self):
        return self.dict_prop.get('RMQuser','')

    def getRMQpassword(self):
        return self.dict_prop.get('RMQpassword','')

    def getRMQexchange(self):
        return self.dict_prop.get('RMQexchange','')

    def getRMQqueue(self):
        return self.dict_prop.get('RMQqueue','')
