#!/usr/bin/python
import time
import datetime
import math
import os
import numpy
import functools
import asyncio
from networktools.colorprint import gprint, bprint, rprint
import threading
from GFAST_buffer import bufferupdatenew, bufferinitnew
from GFAST_locinit import location, location_dict
from GFAST_paraminit import Properties
#from GFAST_alertReader import AMQAlertClient, printer
from GFAST_eewdataengine import data_engine_pgd, data_engine_cmtff
from GFAST_emailer import emailer
import sys
import pika
import json
#############################################################################################################
def client_data(channel):
    print ('lisening to data')
    channel.start_consuming()
def client_eq(channeleq):
    print ('listening to eqs')
    channeleq.start_consuming()
def callbackeqmessage(ch, method, properties, body):
    print ('eq message', body)

class GfastConsumer:
    def __init__(self, *args, **kwargs):
        self.stations = kwargs['stations']
        self.position = kwargs['position']
        self.options = kwargs['array_stations']
        self.data_file = kwargs['data_file']

    def callback(self, ch, method, properties, body):
        #rprint("Loading GFAST callback ")
        global tindex, nbuff, ebuff, ubuff, tbuff, kk
        try:
            kk
        except NameError:
            kk = 1 
        sites = self.options[2]
        sta_lat = self.options[1]
        sta_lon = self.options[0]
        sitesarray = numpy.array(sites)
        stream_length= len(sta_lat)
        if kk == 1:
            [nbuff, ebuff, ubuff, tbuff]=bufferinitnew(Properties('gfast.props'),stream_length)
            kk = 2
        #indsta = numpy.where(sitesarray == 'JRGN')[0]
        #print (sites)
        #gprint("================arrived msg============")
        #rprint(body)
        #bprint(self.stations)
        data = json.loads(body.decode("utf-8") )
        if len(data['data']) == 5:
            n = data['data']['data']['N']['value']
            e = data['data']['data']['E']['value']
            v = data['data']['data']['U']['value']
            t = data['data']['timestamp']
            site = data['data']['station_name']
            indsta = numpy.where(sitesarray == site)[0]
            if (len(indsta) > 0):
                #print (tbuff[0,0]-int(t/1000))
                [nbuff,ebuff,ubuff,tbuff]=bufferupdatenew(indsta,site,nbuff,ebuff,ubuff,tbuff,t,n,e,v,Properties('gfast.props'),stream_length)
            #print (int(t/1000)-tbuff)
            #print (nbuff[:,50])
            if abs(e) > 0.05:
                nprint = "{0:.4f}".format(float(n))
                eprint = "{0:.4f}".format(float(e))
                uprint = "{0:.4f}".format(float(v))
                self.data_file.write(str(site)+' '+str(t)+' ' +
                        nprint+' '+eprint+' '+uprint+'\n')
                #print(t, site, n, e, v)
        # v = data['v'] #vertical
        # n = data['n'] #north
        # e = data['e'] #east
        # t = data['t'] #unix time of record
        #site = data['site']
        # print t,site

        #indsta = numpy.where(sites == site)[0]

        # if (len(indsta) > 0):
        #	[nbuff,ebuff,ubuff,tbuff]=bufferupdatenew(indsta,site,nbuff,ebuff,ubuff,tbuff,t,n,e,v,props)

        # if (time.time()-tindex > 1):
        #	gfast_monitor(sta_lat, sta_lon, props)
        #	tindex = time.time()a


def gfast_monitor(sta_lat,sta_lon,props):
    global eventidlast

    # If there is more than 3 minutes of data, use the station
    a = numpy.where(numpy.nansum(~numpy.isnan(nbuff), axis=1) > 180)[0]

    utc_now = datetime.datetime.utcnow()
    utc_ts = float(utc_now.strftime("%s"))

    eewfile = props.geteewsfile()
    for line in open(eewfile, 'r'):
        cols = line.rstrip()
        cols = line.split()
        SA_eventid = cols[0]
        SA_eventday = cols[1]
        SA_eventtime = cols[2]
        SA_mag = cols[3]
        SA_lon = cols[4]
        SA_lat = cols[5]

    SA_time = time.mktime(datetime.datetime.strptime(
        SA_eventday+'-'+SA_eventtime, "%Y-%m-%d-%H:%M:%S.%f").timetuple())
    SA_time = float(SA_time)
    SA_lat = float(SA_lat)
    SA_lon = float(SA_lon)

    if (utc_ts - SA_time < 300):
        print(utc_ts-SA_time, len(a))

        OUTPUT_PGD = data_engine_pgd(SA_lat, SA_lon, 8.0, SA_time, SA_mag, SA_eventid,
                                     sta_lat[a], sta_lon[a], nbuff[a, :], ebuff[a, :], ubuff[a, :], utc_ts)
        MPGD = OUTPUT_PGD[0]  # PGD Magnitudes as function of depth
        # Variance reduction for PGD as function of depth
        VR_PGD = OUTPUT_PGD[1]
        LEN_PGD = OUTPUT_PGD[2]  # Number of stations used for PGD calculation
        dep_vr_pgd = numpy.argmax(VR_PGD)

        OUTPUT_CMTFF = data_engine_cmtff(SA_lat, SA_lon, 8.0, SA_time, SA_mag, SA_eventid,
                                         sta_lat[a], sta_lon[a], nbuff[a, :], ebuff[a, :], ubuff[a, :], utc_ts)

        #MCMT = OUTPUT_CMTFF[0]
        #VR_CMT = OUTPUT_CMTFF[13]
        #LEN_CMT = OUTPUT_CMTFF[14]
        #dep_vr_cmt = numpy.argmax(VR_CMT)
        # print 'CMT Results'
        # print MCMT[dep_vr_cmt], VR_CMT[dep_vr_cmt],dep_vr_cmt,LEN_CMT
        # print 'PGD Results'
        # print MPGD[dep_vr_pgd], VR_PGD[dep_vr_pgd], dep_vr_pgd, LEN_PGD
        mpgd = "{0:.2f}".format(float(MPGD[dep_vr_pgd]))
        vrpgd = "{0:.2f}".format(float(VR_PGD[dep_vr_pgd]))
        lenpgd = str(LEN_PGD)
        # print mpgd, vrpgd, lenpgd
        f = open(props.geteewgfile(), 'a')
        f.write(str(SA_eventid)+' '+str(SA_lon)+' '+str(SA_lat)+' '+str(SA_time)+' ' +
                str(SA_mag)+' '+mpgd+' '+vrpgd+' '+str(dep_vr_pgd)+' '+lenpgd+' '+str(utc_ts)+'\n')
        f.close()

    if (utc_ts - SA_time > 300):
        if (eventidlast < float(SA_eventid)):
            emailer(SA_eventid)
            eventidlast = float(SA_eventid)


##############################################################################################################
# Start Program
##############################################################################################################
#eventidlast = 2242
#props = Properties('./gfast.props')
#print("Properties %s" % props.dict_prop)
# client=AMQAlertClient(props)
# client.connect(printer)

#[sta_lat,sta_lon,sites]=location(props)
# f = open(props.geteewgfile(), 'a')



def gfast_run_task(props, options):
    threads=[]
    bprint("Gfast Running t0")
    loop=asyncio.get_event_loop()
    [sta_lat, sta_lon, sites]=location_dict(options)
    streamlength = len(sta_lat)
    #[nbuff, ebuff, ubuff, tbuff] = bufferinitnew(props,streamlength)
    credentials = pika.PlainCredentials(props.getRMQuser(), props.getRMQpassword())
    parameters = pika.ConnectionParameters(props.getRMQtopic(),
                                           props.getRMQport(),
                                           props.getRMQhost(),
                                           credentials=credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.exchange_declare(exchange=props.getRMQexchange(), passive=True)
    result = channel.queue_declare(exclusive=True)
    queue_name = result.method.queue
    channel.queue_bind(exchange=props.getRMQexchange(), queue=props.getRMQqueue())
    f = open(props.geteewgfile(), 'a')
    options.update({'array_stations':[sta_lat, sta_lon, sites],'data_file':f})
    consumer = GfastConsumer(**options)
    bprint("New callback %s" %consumer.callback)
    channel.basic_consume(consumer.callback,
                          queue = props.getRMQqueue(),
                          no_ack=True)
    t1 = threading.Thread(target=client_data, args=(channel,))
    t1.daemon = True
    threads.append(t1)
    t1.start()


    RMQequser = 'gfast'
    RMQeqpassword = 'foobarbaz'
    RMQeqexchange = 'amq.fanout'
    RMQeqhost = 'hypo'
    RMQeqtopic = 'shogouki.csn.uchile.cl'
    RMQeqport = '5672'
    credentialseq = pika.PlainCredentials(RMQequser,RMQeqpassword)
    parameterseq = pika.ConnectionParameters(RMQeqtopic,RMQeqport,RMQeqhost,credentials=credentialseq)
    connectioneq = pika.BlockingConnection(parameterseq)

    channeleq = connectioneq.channel()
    channeleq.exchange_declare(exchange=RMQeqexchange,passive=True)
    queue_nameeq = 'gfast'
    channeleq.queue_bind(exchange=RMQeqexchange,queue=queue_nameeq,routing_key='gfast')

    channeleq.basic_consume(callbackeqmessage,queue=queue_nameeq,no_ack=True)
    t2 = threading.Thread(target=client_eq, args=(channeleq,))
    t2.daemon = True
    threads.append(t2)

    async def run_channel():
        t2.start()
        for t in threads:
            t.join()
        #channeleq.start_consuming()
        #channel.start_consuming()
        print ("consuming second")

    if not loop.is_running():
        task = loop.run_until_complete(run_channel())

