#!/usr/bin/python
import time
import math
import numpy
import csv
import asyncio
from networktools.colorprint import gprint, bprint, rprint

def location(props):

    streamfile = props.getstreamfile()
    siteposfile = props.getsiteposfile()
    stream_length = props.getstreamlength()

    LAT = numpy.zeros([int(stream_length), 1])
    LON = numpy.zeros([int(stream_length), 1])
    SITES = numpy.array(range(int(stream_length)),
                        dtype='a5').reshape(int(stream_length), 1)

    k = 0
    for line in open(streamfile, 'r'):
        cols = line.rstrip()
        site = cols[0:4]
        with open(siteposfile, 'rt') as f:
            reader = csv.reader(f, delimiter=',')
            for row in reader:
                if site.lower() == row[0]:
                    LAT[k, 0] = row[8]
                    LON[k, 0] = row[9]
                    SITES[k, 0] = site
        k = k+1

    return (LAT, LON, SITES)


def location_dict(props):
    stations = props['stations']
    position = props['position']
    read_queue = props['read_queue']
    write_queue = props['write_queue']
    trx_bool = True
    LAT = []
    LON = []
    SITES = []

    while trx_bool:
        gprint("Checking if metadata is loaded")
        time.sleep(5)
        if not write_queue.empty():
            for i in range(write_queue.qsize()):
                msg = write_queue.get()
                if msg == 'TRANSFORM':
                    LAT = numpy.zeros([len(stations.keys()), 1])
                    LON = numpy.zeros([len(stations.keys()), 1])
                    bprint("The data will be transformed")
                    k = 0
                    for (ids, station) in stations.items():
                        code = station['code']
                        llh = (position[ids]['llh']['lat'],
                               position[ids]['llh']['lon'])
                        LAT[k, 0] = llh[0]
                        LON[k, 0] = llh[1]
                        SITES.append(code)
                        k += 1
                        trx_bool = False

    return (LAT, LON, SITES)
